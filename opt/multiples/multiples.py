#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tkinter import *
from random import *
from tkinter.ttk import Combobox
#Déclaration de la fenêtre principale
fenetre= Tk()
#Récupération des dimensions de l'écran
larg=(fenetre.winfo_screenwidth())
haut=(fenetre.winfo_screenheight())
#Définition des coordonnées pour centrer la fenêtre
posx=int((larg/2)-500)
posy=int((haut/2)-350)
#Configuration de la fenêtre (dimensions, position et titre)
fenetre.geometry(f"1000x700+{posx}+{posy}")
fenetre.title('Multiples')




img_fond= PhotoImage(file='images/fond.png')

zone=Canvas(fenetre, width=1000, height=700)
zone.place(x=0,y=0)

fond = zone.create_image(0, 0, anchor=NW, image=img_fond)


#Polices
police1 = "-family {DejaVu Sans} -size 22 -weight normal"
police2 = "-family {DejaVu Sans} -size 45 -weight normal"
police3 = "-family {DejaVu Sans} -size 16 -weight normal"



#Variables de départ

x_depart=60
y_depart=150
largeur=35
hauteur=35
suppr=False
ex_aide=0
facteur1=3
facteur2=3


#Fonctions

def afficher(mode, x_depart, y_depart, largeur, hauteur):
    global suppr, varcache, varresultat, varfact1, varfact2, facteur1, facteur2, resultat, xpos, ypos
    creer=0
    efface1=efface2=1
    quest1.configure(background="white")
    quest2.configure(background="white")
    has1.configure(background="white")
    has2.configure(background="white")
    min1.configure(background="white")
    min2.configure(background="white")
    if suppr==True:
        zone.delete("reload")
    if prop.get()==1:
        consigne.configure(text="Toutes valeurs possibles")
    else:
        consigne.configure(text="Valeurs maximales 20 X 15")
    x=x_depart
    y=y_depart
#Mode hasard
    if mode=="hasard":
        quest1.delete(0,END)
        quest2.delete(0,END)
        if has1.get().isdigit() and has2.get().isdigit() and min1.get().isdigit() and min2.get().isdigit() and int(min2.get()) < int(has2.get()) and int(min1.get()) < int(has1.get()):
            facteur1 = randint(int(min1.get()),int(has1.get()))
            facteur2 = randint(int(min2.get()),int(has2.get()))
            if prop.get() == 0 and int(has1.get()) > 20:
                prop.set(1)
            if prop.get() == 0 and int(has2.get()) > 15:
                prop.set(1)
            creer = 1
        else:
            if not has1.get().isdigit():
                has1.delete(0,END)
                has1.configure(background="red")
            if not has2.get().isdigit():
                has2.delete(0,END)
                has2.configure(background="red")
            if not min1.get().isdigit():
                min1.delete(0,END)
                min1.configure(background="red")
            if not min2.get().isdigit():
                min2.delete(0,END)
                min2.configure(background="red")
            if int(min1.get()) > int(has1.get()):
                min1.delete(0, END)
                min1.configure(background="red")
            if int(min2.get()) > int(has2.get()):
                min2.delete(0, END)
                min2.configure(background="red")
            creer=0
#Mode normal
    if mode=="normal":
        if quest1.get().isdigit() and quest2.get().isdigit():
            facteur1 = int(quest1.get())
            facteur2 = int(quest2.get())
            creer=1
        else:
            consigne.configure(text="Entrée non valide")
            if not quest1.get().isdigit():
                quest1.delete(0,END)
                quest1.configure(background="red")
            else:
                efface1=0
            if not quest2.get().isdigit():
                quest2.delete(0,END)
                quest2.configure(background="red")
            else:
                efface2=0
            creer=0
#Mode idem
    if mode=="idem":
        if not quest1.get().isdigit() or not quest2.get().isdigit():
            if not quest1.get() or not quest2.get():
                creer=1
            else:
                consigne.configure(text="Entrée non valide")
                if not quest1.get().isdigit():
                    quest1.delete(0,END)
                    quest1.configure(background="red")
                if not quest2.get().isdigit():
                    quest2.delete(0,END)
                    quest2.configure(background="red")
                creer=0
        elif prop.get() == 0 and (int(quest1.get()) > 20 or int(quest2.get()) > 15) and int(has1.get()) == 20 and int(has2.get()) == 15:
            if int(quest1.get()) > 20:
                quest1.delete(0, END)
                quest1.configure(background="red")
            if int(quest2.get()) > 15:
                quest2.delete(0, END)
                quest2.configure(background="red")
            creer=0
        elif prop.get()==0 and (int(has1.get())>20 or int(has2.get())>15):
            if int(has1.get())>20:
                has1.delete(0,END)
                has1.insert(0,"20")
                facteur1=20
            if int(has2.get())>15:
                has2.delete(0,END)
                has2.insert(1,"15")
                facteur2=15
            creer = 1
        else:
            creer=1
    if efface1==1:
        quest1.delete(0, END)
    if efface2==1:
        quest2.delete(0, END)

#Résultat
    resultat= facteur1*facteur2
#Création des cases
    if (facteur1>20 or facteur2>15) and creer==1:
        prop.set(1)
    if mode=="idem" and suppr==False:
        creer=0
    if creer==1:
        if facteur1 > facteur2:
            nombre = facteur1
        else:
            nombre = facteur2
        if prop.get() == 0:
            largeur = hauteur = 35
        else:
            if facteur1 > 100 or facteur2 > 100:
                if (facteur1 / facteur2) > (20 / 15):
                    largeur = 700
                    hauteur = 700 * (facteur2 / facteur1)
                else:
                    hauteur = 525
                    largeur = 525 * (facteur1 / facteur2)
            else:
                if (facteur1 / facteur2) > (20 / 15):
                    largeur = hauteur = (700 / nombre)
                else:
                    largeur = hauteur = (525 / nombre)
        if facteur1 <= 100 and facteur2 <= 100:
            xpos = ((facteur1 * largeur) / 2) + x
            ypos = ((facteur2 * hauteur) / 2) + y
        else:
            xpos = largeur / 2 + x
            ypos = hauteur / 2 + y
        if facteur1<=100 and facteur2<=100:
            for j in range(facteur2):
                for i in range(facteur1):
                    if colorvar.get()==1 and (i==0 or i==facteur1-1 or j==0 or j==facteur2-1):
                        color="#58a6de"
                    else:
                        color="#80b9e2"
                    case= zone.create_rectangle(x, y, x + largeur, y + hauteur, fill=color, width=ligne.get(), tags="reload")
                    x = x + largeur
                x = x_depart
                y = y + hauteur
        else:
            case = zone.create_rectangle(x, y, x + largeur, y + hauteur, fill="#80b9e2", width=ligne.get(), tags="reload")
#Fin de la création des cases
        if varfact1.get()==1:
            aff_facteur1 = zone.create_text(xpos, y_depart - 25, text=f"{facteur1}", tags="reload", font=police1)
        if varfact2.get() == 1:
            aff_facteur2 = zone.create_text(x_depart-25, ypos - 10, text=f"{facteur2}", tags="reload", font=police1)
        if varfact1.get()==1:
            quest1.insert(1,facteur1)
        if varfact2.get()==1:
            quest2.insert(1,facteur2)
        aff_fois= zone.create_text(x_depart-25, y_depart-25, text="X", tags="reload", font=police1)
        if varcache.get()==1 and 100>facteur1>1 and 100>facteur2>1:
            cache=zone.create_rectangle(x_depart+(largeur/2),y_depart+(hauteur/2),x_depart+(largeur/2)+(facteur1-1)*largeur, y_depart+(hauteur/2)+(facteur2-1)*hauteur, fill="#ecd1fb", tags="reload")
            ex_cache=True
        if varresultat.get()==1:
            aff_resultat=zone.create_text(xpos,ypos,text=f"{resultat}", tags="reload", font=police2)
        suppr=True
#Fin de la boucle de création



def apropos():
    fen_apropos = Toplevel(fenetre, background='#fff')
    fen_apropos.geometry("600x300+300+200")
    fen_apropos.title('À propos')
    texte_manuel = Label(fen_apropos, width=550, wraplength=550, justify='left', background='#fff', text="Multiples est un logiciel qui permet de représenter des multiplications graphiquement par un quadrillage ou une surface.\n\nIl est écrit par Arnaud Champollion et partagé sous licence GNU/GPL.\n\nImage de fond par Nick Roach, licence GNU/GPL.\n\nImage d'aide - tables de multiplications - Marc Degioanni, licence Creative Commons BY SA")
    texte_manuel.pack()
    fen_apropos.mainloop()

def aide():
    global ex_aide, fen_aide
    if ex_aide==0:
        ex_aide = 1
        fen_aide = Toplevel(fenetre, background='#fff')
        fen_aide.geometry("775x588+0+0")
        fen_aide.title('Aide')
        img_escalier= PhotoImage(file='images/escalier.png')
        escalier=Label(fen_aide, image=img_escalier)
        escalier.pack()
        fen_aide.mainloop()
    else:
        ex_aide = 0
        fen_aide.destroy()


#Consignes et entrées utilisateur
varcache = IntVar()
varresultat = IntVar()
varfact1 = IntVar()
varfact2 = IntVar()
colorvar = IntVar()
ligne= IntVar()
prop= IntVar()
varfact1.set(1)
varfact2.set(1)
ligne.set(1)
prop.set(0)

quest1=Entry(zone, justify=CENTER)
quest2=Entry(zone, justify=CENTER)
min1=Entry(zone, justify=CENTER)
min2=Entry(zone, justify=CENTER)
has1=Entry(zone, justify=CENTER)
has2=Entry(zone, justify=CENTER)

bouton_lancer=Button(zone, text="Lancer", command=lambda: afficher("normal", x_depart, y_depart, largeur, hauteur))
bouton_hasard=Button(zone, text="Au hasard", font=police3, command=lambda: afficher("hasard", x_depart, y_depart, largeur, hauteur))

consigne=Label(zone, background="yellow")
consigne.configure(text="Valeurs maximales 20 X 15", font=police3)
quest1.configure(font=police1)
quest2.configure(font=police1)
has1.configure(font=police1)
has2.configure(font=police1)
min1.configure(font=police1)
min2.configure(font=police1)
op_cache=Checkbutton(zone, variable=varcache, text="cache", onvalue = 1, offvalue = 0, command=lambda: afficher("idem", x_depart, y_depart, largeur, hauteur))
op_resultat=Checkbutton(zone, variable=varresultat, text="résultat", onvalue = 1, offvalue = 0, command=lambda: afficher("idem", x_depart, y_depart, largeur, hauteur))
op_fact1=Checkbutton(zone, variable=varfact1, text="fact 1", onvalue = 1, offvalue = 0, command=lambda: afficher("idem", x_depart, y_depart, largeur, hauteur))
op_fact2=Checkbutton(zone, variable=varfact2, text="fact 2", onvalue = 1, offvalue = 0, command=lambda: afficher("idem", x_depart, y_depart, largeur, hauteur))
op_color=Checkbutton(zone, variable=colorvar, text="bords assombris", onvalue = 1, offvalue = 0, command=lambda: afficher("idem", x_depart, y_depart, largeur, hauteur))
op_ligne=Checkbutton(zone, variable=ligne, text="lignes", onvalue = 1, offvalue = 0, command=lambda: afficher("idem", x_depart, y_depart, largeur, hauteur))
op_prop=Checkbutton(zone, variable=prop, text="ajuster", onvalue = 1, offvalue = 0, command=lambda: afficher("idem", x_depart, y_depart, largeur, hauteur))


quest1.place(x=400,y=10, width=50, height=40)
quest2.place(x=500,y=10, width=50, height=40)
bouton_lancer.place(x=600, y=10, height=40)
bouton_hasard.place(x=830, y=360, height=100, width=150)
op_cache.place(x=820, y=110)
op_resultat.place(x=900, y=150)
op_fact1.place(x=900, y=110)
op_fact2.place(x=820, y=150)
min1.place(x=820,y=500, width=50, height=40)
min2.place(x=930,y=500, width=50, height=40)
has1.place(x=820,y=580, width=50, height=40)
has2.place(x=930,y=580, width=50, height=40)
op_color.place(x=820, y=190)
op_ligne.place(x=820, y=230)
op_prop.place(x=820, y=270)



consigne.place(x=25,y=10)
fois = zone.create_text(475, 30, text="X", font=police1)
fois = zone.create_text(900, 522, text="X", font=police1)
fois = zone.create_text(900, 602, text="X", font=police1)


min = zone.create_text(900, 480, text="Valeurs min.")
max = zone.create_text(900, 560, text="Valeurs max.")
texte_ajuster = zone.create_text(890, 320, text="(permet de dépasser\n20x15)")


bouton_apropos=Button(zone, text="À propos", command=apropos)
bouton_apropos.place(x=865, y=660)
bouton_aide=Button(zone, text="Aide", command=aide, bg="#43ff50", font=police1)
bouton_aide.place(x=990, y=10, width=175, height=75, anchor=NE)

#Entrées clavier
fenetre.bind_all('<Right>', lambda event : afficher("hasard", x_depart, y_depart, largeur, hauteur))
fenetre.bind_all('<Left>', lambda event : afficher("hasard", x_depart, y_depart, largeur, hauteur))
fenetre.bind_all('<Return>', lambda event : afficher("normal", x_depart, y_depart, largeur, hauteur))

#Valeurs min max

min1.insert(1, 2)
min2.insert(1, 2)
has1.insert(1, 20)
has2.insert(1, 15)

#Boucle principale
fenetre.mainloop()
